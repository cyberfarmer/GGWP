# GoGo Wordlist Permutator (GGWP)

A utility written in Go inspired by [RSMangler](https://github.com/digininja/RSMangler) which given a wordlist will iterate through common permutations seen in passwords. For use with brute force attack tools such as [Hydra](https://github.com/vanhauser-thc/thc-hydra).

Licensed under GPL v3.0 in the spirit of hacker culture.

Please do not use in military, secret service, policing organizations, or for illegal purposes.

## Usage
```
Usage of GGWP (GoGo Wordlist Permutator)
Note: All mangling options are on by default. Adding an option disables it. Options are marked as [mangle]
  -A, --acronym       Mangle the acronym of the wordlist as well [mangle]
  -a, --append        Append common endings [mangle]
  -i, --input file    Input file to be mangled (default "input.txt")
  -c, --invertcase    Invert the word's case [mangle]
  -L, --leet          1337 speak the word [mangle]
  -l, --lower         All characters lowercase [mangle]
  -n, --numbers       Append & prepend common numbers [mangle]
  -o, --output file   Output mangled file (default "output.txt")
  -p, --prepend       Prepend common beginnings [mangle]
  -s, --separators    Add common separators between words '-_~' [mangle]
  -u, --upper         All characters uppercase [mangle]
  -v, --verbose       Verbose output
  -y, --years         Append & prepend all years from 1970 to current Year [mangle]
  -z, --zero          Turn off all mangling and just permutate
```

## Build Instructions

1. Install Go - [Official Install Documentation](https://go.dev/doc/install)
2. Clone the repo `git clone https://codeberg.org/cyberfarmer/GGWP.git && cd GGWP`
3. Install dependencies `go get .`
4. Build program `go build .`
5. Run the program `./GGWP --help`
