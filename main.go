/*
A wordlist mangler for usage in brute force password testing. Please do not use in military, secret service, policing organizations, or for illegal purposes.
Copyright (C) 2022 "Cyberfarmer" Colin <cyberfarmer_colin@pm.me>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
    "bufio"
    "strconv"
    "log"
    "os"
    "strings"
    "unicode"
    "unicode/utf8"
    "time"
    "fmt"
    flag "github.com/spf13/pflag"
    "github.com/etnz/permute"
)

var (
    //TODO add more mangling options
    verbose bool
    inputFilePath string
    outputFilePath string
    leet bool
    invertCase bool
    appends bool
    prepends bool
    upper bool
    lower bool
    acroFlag bool
    years bool
    numbers bool
    separatorFlag bool
    zero bool
)

func init() {
    //TODO add more mangling options
    flag.BoolVarP(&verbose, "verbose", "v", false, "Verbose output")
    flag.StringVarP(&inputFilePath, "input", "i", "input.txt", "Input `file` to be mangled")
    flag.StringVarP(&outputFilePath, "output", "o", "output.txt", "Output mangled `file`")
    flag.BoolVarP(&leet, "leet", "L", false, "1337 speak the word [mangle]")
    flag.BoolVarP(&invertCase, "invertcase", "c", false, "Invert the word's case [mangle]")
    flag.BoolVarP(&appends, "append", "a", false, "Append common endings [mangle]")
    flag.BoolVarP(&prepends, "prepend", "p", false, "Prepend common beginnings [mangle]")
    flag.BoolVarP(&upper, "upper", "u", false, "All characters uppercase [mangle]")
    flag.BoolVarP(&lower, "lower", "l", false, "All characters lowercase [mangle]")
    flag.BoolVarP(&acroFlag, "acronym", "A", false, "Mangle the acronym of the wordlist as well [mangle]")
    flag.BoolVarP(&years, "years", "y", false, "Append & prepend all years from 1970 to current Year [mangle]")
    flag.BoolVarP(&numbers, "numbers", "n", false, "Append & prepend common numbers [mangle]")
    flag.BoolVarP(&separatorFlag, "separators", "s", false, "Add common separators between words '-_~' [mangle]")
    flag.BoolVarP(&zero, "zero", "z", false, "Turn off all mangling and just permutate")
}

// func to calculate and print execution time
func exeTime() func() {
	start := time.Now()
	return func() {
		log.Printf("Program finished in: %v\n", time.Since(start))
        os.Exit(3)
	}
}

//func to swap cases of each letter
func caseSwap(in string) string {
    var out string = ""

    for _, char := range in {
        if char == unicode.ToUpper(char) {
            out += string(unicode.ToLower(char))
        }

        if char == unicode.ToLower(char) {
            out += string(unicode.ToUpper(char))
        }
    }
    return out
}

//func to append common strings
func commonAppends(in string, results []string) []string {

    //TODO review appends and add/remove to be more inline with common passwords nowadays
    results = append(results, in+"ed")
    results = append(results, in+"ing")
    results = append(results, in+"123")
    results = append(results, in+"1234")
    results = append(results, in+"12345")
    results = append(results, in+"admin")
    results = append(results, in+"adm")
    results = append(results, in+"pwd")
    results = append(results, in+"pw")
    results = append(results, in+"sys")

    return results
}

//func to prepend common strings
func commonPrepends(in string, results []string) []string {

    //TODO review prepends and add/remove to be more inline with common passwords nowadays
    results = append(results, "admin"+in)
    results = append(results, "adm"+in)
    results = append(results, "password"+in)
    results = append(results, "pwd"+in)
    results = append(results, "pw"+in)
    results = append(results, "sys"+in)

    return results
}

//func to convert characters to 1337 versions
func leetSpeak(in string) string {
    var out string = ""
    in = strings.ToLower(in)

    //TODO potentially add multiple leet options for single letter, ie 'l' becomes '1' or '!'
    for _, char := range in {
        switch char {
            case 's':
                out += "$"
            case 'e':
                out += "3"
            case 'a':
                out += "@"
            case 'o':
                out += "0"
            case 'i':
                out += "1"
            case 'l':
                out += "1"
            case 't':
                out += "7"
            case 'b':
                out += "8"
            case 'z':
                out += "2"
            default:
                out += string(char)
        }
    }
    return out
}

//func to generate acronym from inputs
func acronym (inputs []string) string {
    var out string = ""
    for i := range inputs {
        char, _ := utf8.DecodeRuneInString(inputs[i])
        out += string(char)
    }
    return out
}

func main() {

    defer exeTime()()
    dt := time.Now()
    separators := [3]string{"-", "_", "~"}

    flag.Usage = func() {
        fmt.Fprintln(os.Stderr, "Usage of GGWP (GoGo Wordlist Permutator)")
        fmt.Fprintln(os.Stderr, "Note: All mangling options are on by default. Adding an option disables it. Options are marked as [mangle]")
        flag.PrintDefaults()
    }
    flag.Parse()

    //If zero, turn off mangling
    if zero {
        leet = true
        invertCase = true
        appends = true
        prepends = true
        upper = true
        lower = true
        acroFlag = true
        years = true
        numbers = true
        separatorFlag = true
    }

    log.Printf("Running program...")

    if verbose {
        log.Printf("Attempting to load file "+inputFilePath)
    }

    inputFile, err := os.Open(inputFilePath)
    if err != nil {
        log.Fatal(err)
    } else if verbose {
        log.Printf("File loaded successfully")
    }

    scanner := bufio.NewScanner(inputFile)

    inputs := []string{}
    results := []string{}

    for scanner.Scan() {
        var line string = scanner.Text()
        inputs = append(inputs, line)
    }
    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    inputFile.Close()

    if verbose {
        message := "Found "+strconv.FormatInt(int64(len(inputs)), 10)+" words in input file"
        log.Printf(message)
        log.Printf("Combining initial inputs")
    }

    results = append(results, inputs...)

    //Gen acronym before combining og inputs
    acroRes := acronym(inputs)

    tempInputs := []string{}

    //TODO logic in section only does permutations of two and all inputs. Try to get the missing middle at some point
    for i := range inputs {
        for j := range inputs {
            if i != j {
                tempInputs = append(tempInputs, inputs[i]+inputs[j])
                if separatorFlag == false {
                    for k := range separators {
                        tempInputs = append(tempInputs, inputs[i]+separators[k]+inputs[j])
                    }
                }
            }
        }
    }
    results = append(results, tempInputs...)

    heap := permute.NewHeap(len(inputs))
    heapIter := 0
    var swap [2]int
    for heap.Next(&swap) {
        permute.SwapStrings(swap, inputs)
        results = append(results, strings.Join(inputs, ""))
        tempInputs = append(tempInputs, strings.Join(inputs, ""))
        if separatorFlag == false {
            for k := range separators {
                results = append(results, strings.Join(inputs, separators[k]))
                tempInputs = append(tempInputs, strings.Join(inputs, separators[k]))
            }
        }
        heapIter++
    }
    inputs = append(inputs, tempInputs...)

    //Add acronym after combining og inputs
    //Only add to inputs if flag isn't called
    if acroFlag == false {
        inputs = append(inputs, acroRes)
    }
    results = append(results, acroRes)


    if verbose {
        message := "Created "+strconv.FormatInt(int64(len(inputs)), 10)+" unique inputs from original inputs"
        log.Printf(message)
        log.Printf("Mangling...")
    }

    //TODO add more mangling options, optimize flag checking logic?
    //Mangle the things, excluding flags that were passed
    for i := range inputs {
        //The bools on this are weird because idk how pflags/flags works. Logic is, if flag passed, exclude from mangling
        if upper == false {
            results = append(results, strings.ToUpper(inputs[i]))
        }
        if lower == false {
            results = append(results, strings.ToLower(inputs[i]))
        }
        if invertCase == false {
            results = append(results, caseSwap(inputs[i]))
        }
        if leet == false {
            results = append(results, leetSpeak(inputs[i]))
        }
        if appends == false {
            results = commonAppends(inputs[i], results)
        }
        if prepends == false {
            results = commonPrepends(inputs[i], results)
        }
        if years == false {
            currentYear, err := strconv.Atoi(dt.Format("2006"))
            if err != nil {
                log.Fatal(err)
            }
            for j := 0; j <= currentYear-1970; j++ {
                iterYear := strconv.Itoa(1970+j)
                results = append(results, inputs[i]+iterYear)
                results = append(results, iterYear+inputs[i])
            }
        }
        if numbers == false {
            for j := 0; j < 10; j++ {
                iterNum := strconv.Itoa(j)
                results = append(results, inputs[i]+"0"+iterNum)
                results = append(results, "0"+iterNum+inputs[i])
            }
            for j := 0; j <= 123; j++ {
                results = append(results, inputs[i]+strconv.Itoa(j))
                results = append(results, strconv.Itoa(j)+inputs[i])
            }
        }
    }

    if verbose {
        message := "Created "+strconv.FormatInt(int64(len(results)), 10)+" unique outputs"
        log.Printf(message)
    }

    outputFile, err := os.Create(outputFilePath)
    if err != nil {
        log.Fatal(err)
    } else if verbose {
        log.Printf("Output file created successfully")
        log.Printf("Attempting to write to "+outputFilePath)
    }

    writeBuffer := bufio.NewWriter(outputFile)

    for _, line := range results {
        _, err := writeBuffer.WriteString(line+"\n")
        if err != nil {
            log.Fatal(err)
        }
    }

    if err := writeBuffer.Flush(); err != nil {
        log.Fatal(err)
    }

    outputFile.Close()

    if verbose {
        log.Printf("Successfully wrote to output file")
    }
}
